#pragma once

#ifdef LIBRARY_EXPORTS
#define LIBRARY_API __declspec(dllexport)
#else
#define LIBRARY_API __declspec(dllimport)
#endif

#ifdef __cplusplus 
#define LIBRARY_API extern "C" LIBRARY_API 
#endif

LIBRARY_API void api_say_hello();
LIBRARY_API unsigned int api_call_count();
LIBRARY_API int api_sum(int a, int b);

struct vector_s
{
  float x, y;
};

LIBRARY_API float api_vector_size(struct vector_s* vect);
LIBRARY_API void api_vector_normalize(struct vector_s* vect);
