#include "lib.h"
#include <stdio.h>
#include <math.h>

void api_say_hello()
{
  printf("Lib: Hello!\n");
}

unsigned int api_call_count()
{
  static unsigned int count = 0;
  return ++count;
}

int api_sum(int a, int b)
{
  return a + b;
}

float api_vector_size(struct vector_s* vect)
{
	printf("Lib: Calc vector_size for: %f, %f\n", vect->x, vect->y);
	float size = sqrtf(vect->x * vect->x + vect->y * vect->y);
	printf("Lib: Size is: %f\n", size);
	return size;
}

void api_vector_normalize(struct vector_s* vect)
{
	printf("Lib: api_vector_normalize for: %f, %f\n", vect->x, vect->y);
	float size = api_vector_size(vect);
	vect->x /= size;
	vect->y /= size;
	printf("Lib: normalized is: %f, %f\n", vect->x, vect->y);
}
