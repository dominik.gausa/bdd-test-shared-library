Feature: Library Vector

Scenario: Vector Size
  Given Library
  When vector is Vector(4, 4)
  Then vector_size is 5.66

Scenario: Vector Normalize
  Given Library
  When vector is Vector(4, 4)
  And vector is normalized
  Then vector is Vector(0.71, 0.71)
  And vector_size is 1
