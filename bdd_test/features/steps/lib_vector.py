from behave import *
from ctypes import *



class Vector(Structure):
  _fields_ = (
    ("x", c_float),
    ("y", c_float)
  )

  def Dump(self):
    print("Vector is: ")
    for fname, ftype in self._fields_:
      print(fname, getattr(self, fname))




@when(u'vector is Vector({value1},{value2})')
def step_impl(context, value1, value2):
  vect = Vector(float(value1), float(value2))
  vect.Dump()
  context.result = {"vect": vect}

@when(u'vector is normalized')
def step_impl(context):
  context.dll.api_vector_normalize(byref(context.result["vect"]))
  context.result["vect"].Dump()



@Then(u'vector is Vector({value1},{value2})')
def step_impl(context, value1, value2):
  vect = Vector(float(value1), float(value2))
  assert round(getattr(context.result["vect"], "x"), 2) == round(getattr(vect, "x"), 2)
  assert round(getattr(context.result["vect"], "y"), 2) == round(getattr(vect, "y"), 2)

@then(u'vector_size is {value}')
def step_impl(context, value):
  context.dll.api_vector_size.restype = c_float
  size = context.dll.api_vector_size(byref(context.result["vect"]))
  assert float(value) == round(size, 2)
