from behave import *
from ctypes import *

LIB_PATH='../Lib/out/Build/x64-Debug/lib.dll'



@given(u'Library')
def step_impl(context):
  context.dll = cdll.LoadLibrary(LIB_PATH)



@when(u'\'say_hello\' is called')
def step_impl(context):
  context.result = context.dll.api_say_hello()

@when(u'\'call_count\' is called')
def step_impl(context):
  context.result = context.dll.api_call_count()

@when(u'\'sum\' is called with {value1} and {value2}')
def step_impl(context, value1, value2):
  context.result = context.dll.api_sum(int(value1), int(value2))



@then(u'result is {value}')
def step_impl(context, value):
  assert context.result == float(value)

@then(u'pass')
def step_impl(context):
  assert True == True

