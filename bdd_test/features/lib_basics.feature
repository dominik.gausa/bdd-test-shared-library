Feature: Library Basics

Scenario: Say Hello
  Given Library
  When 'say_hello' is called
  Then pass

Scenario: Sum
  Given Library
  When 'sum' is called with 12 and -4
  Then result is 8

Scenario Outline: Call count
  Given Library
  When 'call_count' is called
  Then result is <result>

  Examples: 
    | result |
    | 1|
    | 1|
