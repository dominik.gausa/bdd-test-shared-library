from behave import *
from ctypes import *
from ctypes import wintypes


def CloseDLL(dll):
  handle = dll._handle
  del dll
  kernel32 = WinDLL('kernel32', use_last_error=True)
  kernel32.FreeLibrary.argtypes = [wintypes.HMODULE]
  kernel32.FreeLibrary(handle)


def before_scenario(context, scenario):
  pass

def after_scenario(context, scenario):
  if context.dll:
    CloseDLL(context.dll)

